//import logo from './logo.svg';
import './App.css';
import Search from './api/Search';
import ExpertSearch from './api/ShowSearch'
import Jumbotron from 'react-bootstrap/Jumbotron';
import { Container, Row, Col } from 'react-bootstrap';
import React from 'react';

function App() {
  return (
    <div className="App">
      {<SearchState />}
    </div>
  );
}

class SearchState extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchUrl: ''
    };
    this.childrenUrl = this.childrenUrl.bind(this);
  }

  childrenUrl(url) {
    this.setState({ searchUrl: url });
    console.log('--> state.url: ' + this.state.searchUrl);
  }

  render() {
    return (
      <div>
        <Jumbotron fluid>
          <Container>
            <Row>
              <Col md={7}><h1>Welcome to Deports House</h1></Col>
              <Col md={4}><Search setUrl={this.childrenUrl} /></Col>
            </Row>
          </Container>
        </Jumbotron>
        <div>
          {<ExpertSearch nameProduct={this.state.searchUrl} />}
        </div>
      </div>
    );
  }
}

export default App;
