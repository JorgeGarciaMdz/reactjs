import React from 'react';
//import { Container, Row, Col } from 'react-bootstrap';
import ListGroup from 'react-bootstrap/ListGroup';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';

function Search(props) {
  return (
    <div>
      {<FetchSearch superUrl={props.setUrl} />}
    </div>
  );
}

class FetchSearch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        products: [],
        brands: [],
        sports: []
      }, keyword: ''
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleUrl = this.handleUrl.bind(this);
    this.handleButton = this.handleButton.bind(this);
    this.clearData = this.clearData.bind(this);
  }

  handleInput(event) {
    const url = 'http://127.0.0.1:3000/api/v1/searchWords?keyword=';
    this.setState({ keyword: event.target.value });

    fetch(url + event.target.value )
      .then((response) => {
        return response.json();
      })
      .then((data) => { this.setState({ data: data.data }) });

    if (this.state.data.products.length === 0) {
      this.setState(
        {
          data: {
            products: [],
            brands: [],
            sports: []
          }
        }
      );
    }
  }

  handleUrl(event) {
    const keysearch = event.target.value.split('?');
    this.props.superUrl(keysearch[1]);
    this.clearData();
  }

  handleButton(){
    this.props.superUrl('keywords=' + this.state.keyword);
    this.clearData();
  }

  clearData(){
    this.setState({
      data: {
        products: [],
        brands: [],
        sports: []
      },
      keyword: ''
    });
  }

  render() {
    return (
      <div>
        <div>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <Button variant="outline-secondary" onClick={this.handleButton}>Buscar </Button>
              <FormControl aria-describedby="basic-addon1" placeholder="que buscas?"
                value={this.state.keyword} onChange={this.handleInput}
              />
            </InputGroup.Prepend>
          </InputGroup>
        </div>
        <div>
          <ItemProduct products={this.state.data.products} handleUrl={this.handleUrl} />
          <ItemBrands brands={this.state.data.brands} handleUrl={this.handleUrl} />
        </div>
      </div>
    );
  }
}

function ItemProduct(props) {
  if (props.products.length > 0) {
    const liProducts = props.products.map((product, index) => {
      return (
        <ListGroup.Item action key={index} value={product.url} onClick={props.handleUrl}>
          {product.name}
        </ListGroup.Item>
      );
    })
    return (
      <ListGroup>
        <ListGroup.Item action key={'producto'} active>
          <b><h4>Producto</h4></b>
        </ListGroup.Item>
        {liProducts}
      </ListGroup>
    );
  } else {
    return null;
  }
}

function ItemBrands(props) {
  if (props.brands.length > 0) {
    const liBrands = props.brands.map((brand, index) => {
      return (
        <ListGroup.Item action key={index} value={brand.urls.city} onClick={props.handleUrl}>
          {brand.name}
        </ListGroup.Item>
      );
    });
    return (
      <ListGroup>
        <ListGroup.Item action key={'marca'} active>
          <b><h4>Marca</h4></b>
        </ListGroup.Item>
        {liBrands}
      </ListGroup>
    );
  } else {
    return null;
  }
}

export default Search;