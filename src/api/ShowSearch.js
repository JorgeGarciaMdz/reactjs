import React from 'react';
import CardColumns from 'react-bootstrap/CardColumns';
import Card from 'react-bootstrap/Card';

class ExpertShow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      keysearch: '',
      products: {
        items: []
      }
    }
  }

  componentDidUpdate() {
    console.log('llamando didUpdate');
    if (this.state.keysearch === this.props.nameProduct) {
      return null;
    } else {
      this.setState({
        keysearch: this.props.nameProduct
      });
      this.fetchProducts();
      return null;
    }
  }

  fetchProducts() {
    console.log('fetchProducts + ' + this.props.nameProduct);
    if (this.props.nameProduct.length > 5) {
      console.log('http://127.0.0.1:3000/api/v1/searchProducts?' + this.props.nameProduct);
      const url = 'http://127.0.0.1:3000/api/v1/searchProducts?';
      fetch(url + this.props.nameProduct)
        .then((response) => response.json())
        .then((data) => {
          console.log('data: ' + JSON.stringify(data));
          if (data.items) {
            this.setState({
              products: {
                items: data.items
              }
            });
          }
        })
        .catch((err) => console.log('err: ' + err));
    }
    return null;
  }

  render() {
    return (
      <div>
        { /*<div>
          ShowSearch search?{this.state.keysearch}<br />
          if update search? {this.props.nameProduct} <br />
          lengh keysearch: {this.state.keysearch.length} <br />
          count items of products: {this.state.products.items.length}
        </div> */}
        <div>
          <ItemsProducts items={this.state.products.items} />
        </div>
      </div>
    );
  }
}

function ItemsProducts(props) {

  const li = props.items.map((item, index) => {
    return (
      <Card border="light" style={{ width: '18rem' }} key={item.sku}>
        <Card.Header>{item.brand_name}  {item.gender}</Card.Header>
        <Card.Body>
          <Card.Title>{item.name}</Card.Title>
          {/*<Card.Text>
            Some quick example text to build on the card title and make up the bulk
            of the card's content.
          </Card.Text> */}
          <Card.Img variant="bottom" src={item.dummy_images[0].url} />
        </Card.Body>
        <Card.Footer>
          <small className="text-muted">1 pago de: <b>${item.all_prices.original}</b></small><br/>
          <small className="text-muted">{item.all_prices.installments.quantity} cuotas de: <b>${item.all_prices.installments.value}</b></small>
          <br />
          <a href={"https://www.vaypol.com.ar/city/" + item.url}>
            <small><b>Ver</b></small>
          </a>
        </Card.Footer>
      </Card>
    );
  });
  return (
    <CardColumns>
      {li}
    </CardColumns>
  );
}

export default ExpertShow;
